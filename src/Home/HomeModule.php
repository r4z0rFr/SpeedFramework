<?php
namespace App\Home;

use Framework\Renderer;
use Framework\Router;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class HomeModule
{

    private $renderer;

    public function __construct(Router $router, Renderer $renderer)
    {
        $this->renderer = $renderer;
        $renderer->addPath('index', __DIR__ . '/views');
        $router->get('/', [$this, 'index'], 'home.index');
    }

    public function index(Request $request): string
    {
        return $this->renderer->render('index');
    }
}
