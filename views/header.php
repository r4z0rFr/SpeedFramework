<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Sp33dFramework</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="FirstPage">
    <meta name="author" content="Sp33dy">

    <!-- Load Font -->
    <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">


    <!-- Bootstrap Css-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

</head>
<header>
    <nav class="navbar navbar-default navbar-fixed-top " style="font-family: 'Indie Flower', cursive;">
        <div class="container-fluid">
            <a class="navbar-brand" href="#"><strong>SpeedFramework</strong></a>
            <p class="navbar-text pull-right">By <a href="#" class="navbar-link">Sp33dy</a></p>
        </div>
    </nav>
</header>
<div style="padding-bottom: 60px"></div>